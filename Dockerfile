FROM debian:stretch
MAINTAINER Andrew Dunham <andrew@du.nham.ca> Alexander Sack <asac129@gmail.com>

# Install build tools
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -yy && \
    DEBIAN_FRONTEND=noninteractive apt-get install -yy \
        automake            \
        bison               \
        build-essential     \
        curl                \
        file                \
        flex                \
        git                 \
        libtool             \
        pkg-config          \
        python              \
        texinfo             \
        vim                 \
        wget

# Install musl-cross
RUN mkdir /build &&                                                 \
    cd /build &&                                                    \
    git clone https://github.com/GregorR/musl-cross.git &&          \
    cd musl-cross &&                                                \
    echo 'ARCH=mipsel'                                 >> config.sh && \
    echo 'TRIPLE=mipsel-linux-musl'              >> config.sh && \
    echo 'WITH_SYSROOT=yes'                         >> config.sh && \
    echo 'GCC_BUILTIN_PREREQS=yes'                  >> config.sh && \
    sed -i -e "s/^MUSL_VERSION=.*\$/MUSL_VERSION=1.1.14/" defs.sh &&  \
    ./build.sh &&                                                   \
    cd / &&                                                         \
    apt-get clean &&                                                \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /build

ENV PATH $PATH:/opt/cross/mipsel-linux-musl/bin
CMD /bin/bash
